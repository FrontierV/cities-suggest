import fetchSuggests from './api';
import debounce from './debounce';

let controller;
let isRequestStarted = false;

export default {
  state: {
    suggestions: [],
  },

  actions: {
    getSuggestions: debounce(async ({ dispatch, commit }, query) => {
      const suggestions = await dispatch('fetchSuggests', query);

      if (!suggestions) {
        return;
      }

      commit('setSuggestions', suggestions);
    }, 300),

    async fetchSuggests(ctx, query) {
      let response;

      if (isRequestStarted) {
        controller.abort();
      }

      controller = new AbortController();
      isRequestStarted = true;

      try {
        response = await fetchSuggests(query, controller);
      } catch (e) {
        if (e.name !== 'AbortError') {
          // не обрабатываем ошибки генерируемой прерыванием запроса
          throw new Error('Произошла ошибка');
        }
      } finally {
        isRequestStarted = false;
      }

      return response;
    },

    // отчищаем подсказки если запрос еще не вернулся, а пользователь уже отчистил ввод
    clear({ commit }) {
      controller.abort();
      isRequestStarted = false;

      commit('setSuggestions', []);
    },
  },

  mutations: {
    setSuggestions(state, suggestions) {
      state.suggestions = suggestions
        .map((suggestion) => ({
          id: suggestion.id,
          title: suggestion.name,
          description: suggestion.displayText,
        }))
        .slice(0, 10);
    },
  },
};
