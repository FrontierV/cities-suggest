import Vue from 'vue';
import Vuex from 'vuex';
import storeModule from './store';
import App from './App.vue';

Vue.config.productionTip = false;
Vue.use(Vuex);

const store = new Vuex.Store(storeModule);

new Vue({
  store,
  render: (h) => h(App),
}).$mount('#app');
