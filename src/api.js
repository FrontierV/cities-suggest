export default async (query, controller) => {
  const response = await fetch(`https://cors-anywhere.herokuapp.com/https://www.sportmaster.ru/rest/v1/address?query=${query}`, {
    signal: controller.signal,
  });

  return response.json();
};
