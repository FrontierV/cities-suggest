export default function debounce(fn, ms) {
  let timeout;
  let isCountdownOn = false;
  const createTimeout = (args) => setTimeout(() => {
    isCountdownOn = false;
    fn.apply(this, args);
  }, ms);

  return (...args) => {
    if (isCountdownOn) {
      clearTimeout(timeout);
    }

    isCountdownOn = true;
    timeout = createTimeout(args);
  };
}
